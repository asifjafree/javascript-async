function fetchRandomNumbers() {
  return new Promise((resolve, reject) => {
    console.log('Fetching number...');
    setTimeout(() => {
      let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
      console.log('Received random number:', randomNum);
      resolve(randomNum);
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
  });
}

function fetchRandomString() {
  return new Promise((resolve, reject) => {
    console.log('Fetching string...');
    setTimeout(() => {
      let result = '';
      let characters =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      let charactersLength = characters.length;
      for (let i = 0; i < 5; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        );
      }
      console.log('Received random string:', result);
      resolve(result);
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
  });
}

fetchRandomNumbers((randomNum) => console.log(randomNum));
fetchRandomString((randomStr) => console.log(randomStr));
fetchRandomNumbers().then((randomNum) => {
  console.log(randomNum);
});
fetchRandomString().then((randomStr) => {
  console.log(randomStr);
});

function taskTwo() {
  let sum = 0;
  fetchRandomNumbers().then((randomNum) => {
    sum += randomNum;
    console.log(sum);
    fetchRandomNumbers().then((randomNum) => {
      sum += randomNum;
      console.log(sum);
    });
  });
}
function taskThree() {
  let str = '';
  fetchRandomNumbers().then((randomNum) => {
    str += randomNum;
    fetchRandomString().then((randomStr) => {
      str += randomStr;
      console.log(str);
    });
  });
}
function taskFour() {
  let sum = 0;
  fetchRandomNumbers().then((randomNum) => {
    sum += randomNum;
    fetchRandomNumbers().then((randomNum) => {
      sum += randomNum;
      fetchRandomNumbers().then((randomNum) => {
        sum += randomNum;
        fetchRandomNumbers().then((randomNum) => {
          sum += randomNum;
          fetchRandomNumbers().then((randomNum) => {
            sum += randomNum;
            fetchRandomNumbers().then((randomNum) => {
              sum += randomNum;
              fetchRandomNumbers().then((randomNum) => {
                sum += randomNum;
                fetchRandomNumbers().then((randomNum) => {
                  sum += randomNum;
                  fetchRandomNumbers().then((randomNum) => {
                    sum += randomNum;
                    fetchRandomNumbers().then((randomNum) => {
                      sum += randomNum;
                      console.log(sum);
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

taskTwo();
taskThree();
taskFour();
